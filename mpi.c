#include <assert.h>
#include <mpi.h>

/**
 *  Broadcast the data using the minimum spanning tree algorithm,
 *  i.e., rank 0 has the initial data and we recursively double the
 *  number of communicating processes over log_2(P) rounds.
 *
 *  \pre Number of processes, P, must divide the message length, len.
 */
void bcast (int* data, const int len)
{
  MPI_Bcast (data, len, MPI_INT, 0, MPI_COMM_WORLD);
}

const char* bcast_algorithm (void)
{
  static const char* name = "mpi";
  return name;
}

/* eof */
